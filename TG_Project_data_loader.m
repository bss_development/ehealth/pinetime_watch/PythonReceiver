% Example script to load ppg and motion data from .csv file. Converts
% timestamps from yyyy-mm-dd-ss to seconds.
% written by R. Poelarends / University of Twente

% released under CC-BY License, see https://creativecommons.org/licenses/by/4.0/

%Major issues:
% - Insert path to data manually

clear all, clc

%% Give paths of data
%%% EDIT BELOW %%%
folder = ""; % Path to folder that contains the data (should end with \ )
%%% EDIT ABOVE %%%

ppg_file = "ppg.csv";
motion_file = "motion.csv";

%% Read data from files
ppg = readtable(folder+ppg_file, 'Delimiter', ',');
motion = readtable(folder+motion_file,'Delimiter', ',');
clear folder ppg_file motion_file
%% Convert table to arrays
ppg_time = ppg.Time;
ppg_signal = ppg.ppg;

motion_time = motion.Time;
motion_x = motion.x;
motion_y = motion.y;
motion_z = motion.z;

%% Convert time
ppg_time = datetime(ppg_time, "Format", "yyyy-MM-dd HH:mm:ss.SSSSSSSSSXXX", "TimeZone", "UTC+1");
motion_time = datetime(motion_time, "Format", "yyyy-MM-dd HH:mm:ss.SSSSSSSSSXXX", "TimeZone", "UTC+1");

ppg_time = ppg_time - ppg_time(1);
motion_time = motion_time - motion_time(1);

ppg_time = seconds(ppg_time);
motion_time = seconds(motion_time);