import asyncio
import os
import time
import datetime
import csv
import keyboard
from colorama import Fore
import pandas as pd
import bleak
from halo import Halo

#########################################################
##Decive Bluetooth MAC address 
device_MAC ='E0:7D:38:D7:D4:E8' 
#########################################################

motionDF=pd.DataFrame(columns=["Time", "X", "Y", "Z"])
ppgDF=pd.DataFrame(columns=["Time", "ppg"])

motionScaleFactor = 8 #the watch assumes 2G range, but it is modified to be at 16G range, therefore a scaling is required

spinner = Halo(text='Loading', spinner={'interval': 100,'frames': ["⣾","⣷","⣯","⣟","⡿","⢿","⣻","⣽"]})

Old_ppg_t=0
Old_ppg =[]
waitTime = 9 #seconds


async def run(address, service_uuid_1, service_uuid_2, service_uuid_3, directory, motionSaveName, ppgSaveName, bpmSaveName):
    
    async with bleak.BleakClient(address) as client:
        await client.is_connected()
        await client.start_notify(service_uuid_1, lambda sender, data: handle_motion(sender, data, directory, motionSaveName))
        await client.start_notify(service_uuid_2, lambda sender, data: handle_ppg(sender, data, directory, ppgSaveName))
        await client.start_notify(service_uuid_3, lambda sender, data: handle_bpm(sender, data, directory, bpmSaveName,bpm_value))
        # Store the time of the last received notification
        last_notif_time = time.time()
        bpm_value = None
        try:

            print (Fore.BLUE+ "[INFO]" +Fore.RESET+ " Press Ctrl+s to finish the data collection") 
            spinner.start('Parsing Data')

            while True:
                
                await asyncio.sleep(0.1)

                if keyboard.is_pressed('ctrl+s'):  # Stop loop on 'CTRL+s' key press
                    spinner.stop()
                    print (Fore.BLUE+ "[INFO]" +Fore.RESET+ " Ending data Collection")
                    #load raw csv files
                    motionDF=pd.read_csv(os.path.join(directory, motionSaveName))
                    ppgDF=pd.read_csv(os.path.join(directory, ppgSaveName))

                    #convert time to epoch to UTC
                    motionDF['Time'] = pd.to_datetime(motionDF['Time'], unit='s',  utc=True)
                    motionDF.index = motionDF['Time']
                    del motionDF['Time']

                    ppgDF['Time'] = pd.to_datetime(ppgDF['Time'], unit='s',  utc=True)
                    ppgDF.index = ppgDF['Time']
                    del ppgDF['Time']

                    #Change TimeZone
                    motionDF = motionDF.tz_convert("Europe/Amsterdam")
                    ppgDF = ppgDF.tz_convert("Europe/Amsterdam")
                    
                    #resample data 
                    motionDF_int=motionDF.resample('10ms').mean()

                    #interpolate columns
                    for ax in ['x','y','z']:
                        motionDF_int[ax]=motionDF_int[ax].interpolate(method='spline', order=3)#.astype('int')
                    
                    #save new dataframes
                    path=os.path.split(directory)[:-1]
                    motionDF_int.to_csv(os.path.join(''.join(path),'motion.csv'))
                    ppgDF.to_csv(os.path.join(''.join(path),'ppg.csv'))


                    break
                
                
                # global Old_ppg_t
                # await asyncio.sleep(Old_ppg_t+waitTime)
                # # Check if there is new data in the last 3 seconds
                # if Old_ppg_t is not None and time.time() - Old_ppg_t > waitTime:
                #     # Read data from the characteristic
                #     ppg_data = await client.read_gatt_char(service_uuid_2)
                #     # Pass the received data to the handler function
                #     handle_ppg(sender="requested", data=ppg_data, directory=directory, ppgSaveName=ppgSaveName)
                #     Old_ppg_t = time.time()
        
        except KeyboardInterrupt:
            (Fore.BLUE+ "[INFO]" +Fore.RESET+ " Keyboard Interrupt")


def handle_motion(sender, data, directory, motionSaveName,**kargs):
    motion = memoryview(data).cast('h')
    mot_t = time.time()
    motionDF.loc[len(motionDF)] = [mot_t, motion[0], motion[1], motion[2]]
    append_to_file(directory, motionSaveName, [mot_t, motion[0]*motionScaleFactor, motion[1]*motionScaleFactor, motion[2]*motionScaleFactor])  
    spinner.start('Parsing Data  | last x : {}, y : {}, z: {}'.format(motion[0]*motionScaleFactor, motion[1]*motionScaleFactor, motion[2]*motionScaleFactor))

def handle_ppg(sender, data, directory, ppgSaveName,**kargs):
    if sender=="requested":
        print (Fore.BLUE+ "[INFO]" +Fore.RESET+ " Requested package")

    ppg = memoryview(data).cast('b')

    if ppg == Old_ppg:
       print (Fore.BLUE+ "[INFO]" +Fore.RESET+ " ppg equal than previous package") 
    global Old_ppg_t
    New_ppg_t=time.time()

    if Old_ppg_t==None:
        Old_ppg_t = New_ppg_t - 8.3333333

    

    t_dif=New_ppg_t-Old_ppg_t
    if t_dif > 12:
        spinner.warn(" Missed PPG Data Block: {}".format(time.strftime("%Hh %M:%S")))
        spinner.start('Parsing Data')
        Old_ppg_t = New_ppg_t - 8.3333333
        t_dif=New_ppg_t-Old_ppg_t

    i=0
    for p in ppg:
        i=i+1
        t=Old_ppg_t+i*t_dif/200
        append_to_file(directory, ppgSaveName, [t,p])
        ppgDF.loc[len(ppgDF)] = [t, p]

    Old_ppg_t=New_ppg_t

def handle_bpm(sender, data, directory, bpmSaveName, bpm_value,**kargs):
    bpm=memoryview(data).cast('B')
    bpm_t=time.time()

     
    # make sure spinner is a Spinner object before updating its text
    spinner.start('Parsing Data  | last bpm : {}'.format(bpm[1]))



def append_to_file(directory, filename, data):
    filepath = os.path.join(directory, filename)
    with open(filepath, 'a',newline='') as f:
            writer = csv.writer(f)
            # Write the new data to the CSV file    
            writer.writerow(data)

if __name__ == "__main__":

    timestr = time.strftime("%Y_%m_%d-%H_%M_%S")

    #Bluetooth service address DO NOT CHANGE
    Motion_UUID ='00030002-78fc-48fe-8e23-433b3a1942d0'
    Ppg_UUD     ='0000fafa-0000-1000-8000-00805f9b34fb'
    bpm_UUD     ='00002A37-0000-1000-8000-00805f9b34fb'


    motionSaveName ="motion.csv"
    ppgSaveName="ppg.csv"
    bpmSaveName= "bpm.csv"
 
    if not os.path.exists('Output'):
        os.makedirs('Output')
    
    
    directory=os.path.join('Output/',timestr,'Raw')
    os.makedirs(directory)

    file_list=[ppgSaveName,bpmSaveName,motionSaveName]
    header_list=[["Time","ppg"],["Time","bpm"],["Time","x","y","z"]]
    for file,header in zip(file_list,header_list):
        with open (os.path.join(directory,file),"w",newline='') as f:
            writer = csv.writer(f)
            writer.writerow(header)
            


    loop = asyncio.get_event_loop()
    try:
        #loop.run_until_complete(run(device_MAC, Motion_UUID, Ppg_UUD, bpm_UUD, directory, motionSaveName, ppgSaveName, bpmSaveName))
        loop.run_until_complete(run(device_MAC, Motion_UUID, Ppg_UUD, bpm_UUD, directory, motionSaveName, ppgSaveName, bpmSaveName))
    except bleak.exc.BleakError:
        print(Fore.YELLOW+ "[WARNING]" +Fore.RESET+ " Watch NOT FOUND")
        print('          Tips: Are you using the right MAC address?')
        print('                Keep the screen of the watch open while trying to connect.')
        print('                Stop the PPG sensor while trying to connect.')
        print('                Maybe just try again. Some times it fails :)')
    
    # loop.run_forever()
