# Python receiver Win11 compatible



## Getting started


To run the script you must have python 3 installed in your laptop and configured to the PATH (usually is done automatically while installing python). 
If in power shell / cmd you write:

``python --version``

if  it responds with python '3.xx.xx' you are good to go

The first time you run the code you may be missing some libraries.
execute:

```pip install -r requirements.txt```

to download them (from the directory you have the files)


Update the Mac address to match the one from the Pine watch you want to pair.
You can find the address written in the sticker of the watch;
if you hold the button for 3s it will open a menu with information where the MAC address will also be written. 

The MAC address must be edited in the py code, at line ~14: 
```device_MAC ='F1:AB:42:66:8A:B8' ```
change this into the address of your watch.
You do not need to add the watch in the Bluetooth settings of Windows / Linux.

Sampling will start automatically upon starting the script. You can stop sampling by pressing Ctrl+S

After sampling is stopped, the accelerometer data will be resampled to 100 Hz. 
All data will be stored in a subfolder "Output" at the location of the py script. 

